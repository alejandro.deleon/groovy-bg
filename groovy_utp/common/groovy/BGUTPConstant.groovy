#!/usr/bin/env groovy

class BGUTPConstant{
  def public static String groovy_batch = System.getenv("GROOVY_BATCH")
  def public static String bgCommonLogFile = groovy_batch + '/common/config/BGUTPLogConfig.cfg'
  def public static String bgCommonConfigFile = groovy_batch + '/common/config/BGUTPConfig.cfg'
  def public static String[] bgDbProviders = [
    'MySQL',
    'SQLServer',    
    'Sybase',
    'SybaseIQ'
  ]

}
