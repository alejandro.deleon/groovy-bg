#!/usr/bin/env groovy

import org.apache.log4j.*
import groovy.util.logging.*

import BGUTPConstant

@Log4j
class BGUTPLogger {
  def public static Logger getLogger ( String appName) {
    return getLogger( appName, 'ERROR')
  }
  def public static Logger getLogger ( String appName, String level) {
    try {
      def config = new ConfigSlurper().parse(new File(BGUTPConstant.bgCommonLogFile).toURL())
      config.log4j.appender."scrlog.File"= BGUTPConstant.groovy_batch + "/logs/" + appName +".log"
      switch ( level ){
        case 'INFO' :
          config.log4j.rootLogger="info,scrlog"
          config.log4j.logger.ProcessLogger="info,scrlog"
          break
        case 'DEBUG' :
          config.log4j.rootLogger="debug,scrlog"
          config.log4j.logger.ProcessLogger="debug,scrlog"
          break
        default:
          config.log4j.rootLogger="error,scrlog"
          config.log4j.logger.ProcessLogger="error,scrlog"
          break
      }
      
      PropertyConfigurator.configure(config.toProperties())    
      Logger log = Logger.getInstance(getClass())
      return log
    }catch(Exception ex) {
      println "BGUTPLogger getLogger: ${ex}"
      System.exit(1)
    }

  }
}

