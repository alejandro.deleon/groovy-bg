#!/usr/bin/env groovy

import groovy.sql.Sql
import groovy.json.JsonSlurper
import org.apache.commons.dbcp2.BasicDataSource
import java.sql.SQLException
import java.sql.Connection
import BGUTPConstant

class BGUTPConexion {
  def static BasicDataSource getDataSource(String dsName) throws SQLException {
    return _getDataSource(dsName, '', '', '')
  }
  def static BasicDataSource getDataSource(String sa_user, String sa_password, String token) throws SQLException {
    return _getDataSource('', sa_user, sa_password, token)
  }

  def private static BasicDataSource _getDataSource(String dsName, String sa_user, String sa_password, String token) throws SQLException {
    /* DATASOURCE*/
    try{
      def String fileName = BGUTPConstant.bgCommonConfigFile
      def BasicDataSource ds = new BasicDataSource()
      def ConfigObject config = new ConfigSlurper().parse(new File(fileName).toURL())
      def list = new JsonSlurper().parseText( config.dataSource )
      list.each { dbCentral ->

        if ( dbCentral.name == dsName || ( dsName == '' && dbCentral.token == token ) ){
 
          ds.setUsername(dbCentral.user)
          ds.setPassword(dbCentral.password)
          ds.setDriverClassName(dbCentral.driver)
          ds.setUrl(dbCentral.url)
          ds.setDefaultTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED)
          ds.setDefaultAutoCommit(true)
          if (null != dbCentral.initialSize ){
            ds.setInitialSize(dbCentral.initialSize)
          }
          if (null != dbCentral.maxTotal ){
            ds.setMaxTotal(dbCentral.maxTotal)
          }
          if (null != dbCentral.maxIdle ){
            ds.setMaxIdle(dbCentral.maxIdle)
          }
          return ds
        }
      }
      return ds
    }catch (SQLException sqlex){
      throw sqlex
    } catch(Exception ex) {
      throw ex
    }
  }

}

