DELIMITER $$
USE sample
$$
drop table IF  EXISTS counter;
$$
drop table IF  EXISTS catalogo;
$$
drop table IF  EXISTS cuentas;
$$
drop table IF  EXISTS tasas;
$$

create table IF NOT EXISTS counter (
currentId decimal(10,0))ENGINE= INNODB;
$$
insert counter values(0);
$$
create table IF NOT EXISTS cuentas (
secuencial  integer,
cuenta      varchar(30),
producto    varchar(10),
saldo       decimal(13,2),
intereses   decimal(13,2),
fecha_crea  datetime(6),
fecha_mod  datetime(6) null,
fecha_calc datetime(6) null,
user_crea   varchar(20),
user_mod     varchar(20)

) ENGINE= INNODB;
$$
create unique index  cuentas_key1 on cuentas  (secuencial);
create unique index  cuentas_key2 on cuentas  (cuenta);
$$
create table IF NOT EXISTS catalogo ( id varchar(10), descripcion varchar(30)) ENGINE =INNODB;
$$
create unique index catalogo_key1 on catalogo(id);
$$
truncate table catalogo;
$$
insert into catalogo (id, descripcion) values ("AHO", "CUENTAS DE AHORROS");
$$
insert into catalogo (id, descripcion) values ("CTE", "CUENTAS CORRIENTES");
$$
insert into catalogo (id, descripcion) values ("PFI", "PLAZO FIJO");
$$
insert into catalogo (id, descripcion) values ("PRE", "PRESTAMOS");
$$
insert into catalogo (id, descripcion) values ("VISA", "TARJETAS VISA");
$$
create table IF NOT EXISTS tasas ( productId varchar(10), tasa decimal(10,6)) ENGINE =INNODB;
$$
insert into tasas (productId, tasa) values ('AHO',1.00);
$$
insert into tasas (productId, tasa) values ('CTE',0.00);
$$
insert into tasas (productId, tasa) values ('PRE',4.00);
$$
insert into tasas (productId, tasa) values ('PFI',3.00);
$$
insert into tasas (productId, tasa) values ('VISA',10.00);
$$
drop procedure if exists sp_prueba_groovy;
$$
CREATE PROCEDURE sp_prueba_groovy( t_entrada varchar ( 32 ) )
sp_lbl:
begin
  
select t_entrada as t_entrada, 
       id as id, 
       descripcion as descripcion
       from catalogo;
  
end
$$
drop procedure if exists sp_secuencial;
$$ 
create procedure sp_secuencial
 ( OUT o_id decimal(18,0) )
sp_lbl:
begin
control_lbl:
begin

  
  declare w_id_generado decimal(18,0) default 0;
  declare w_rows        int default 0;
  declare w_error       int default 0;
  declare w_contor      int default 0;
  set o_id = -1;

  
  repeat
    select currentId into w_id_generado 
    from counter
    limit 1;
    
    update   counter 
    set    currentId = currentId + 1
    where  currentId = w_id_generado
    limit 1;
     
    set w_rows  = row_count();
    set w_contor = w_contor +1;
    set w_id_generado = w_id_generado + 1;
  until w_rows > 0 or w_contor > 100 end repeat;
  
 
  if w_rows <> 1 then

      leave control_lbl;
  end if;
  
  set o_id = w_id_generado;
  leave sp_lbl;
  end;
  leave sp_lbl;


end;
$$

drop procedure if exists sp_insert_prueba_groovy;
$$
CREATE PROCEDURE sp_insert_prueba_groovy( i_estudiante varchar ( 20 ), i_producto varchar(10), out o_retorno int)
sp_lbl:
begin

	declare w_id_generado decimal(18,0);
	declare w_cuenta varchar(30); 

	set o_retorno = 0;  

	call sp_secuencial(w_id_generado);
	set w_cuenta = concat(upper(i_estudiante),"-",w_id_generado);
	insert cuentas(secuencial,cuenta,producto,saldo,intereses,fecha_crea,fecha_mod,fecha_calc,user_crea,user_mod )
	values(w_id_generado,w_cuenta,i_producto,100,0,now(),now(), date_add( current_date(),interval -1 day),i_estudiante,i_estudiante);
	if row_count() <> 1 then
	   set o_retorno = 9999;
	end if;
  
end
$$

drop procedure if exists sp_calculo_interes_groovy;
$$
CREATE PROCEDURE sp_calculo_interes_groovy( i_estudiante varchar ( 20 ), out o_retorno int)
sp_lbl:
begin

	declare w_interes decimal(18,2);
	declare w_cuenta varchar(30); 
    declare w_secuencial  integer;
    declare w_tasa  decimal (10,6);
    declare w_saldo decimal(18,2);
    declare w_no_more_rows int default 0;
    
	declare cuentas_cursor cursor
						  for select  secuencial,cuenta,tasa,saldo
							  from   cuentas, tasas
							  where  producto= productId
							  and    user_crea = i_estudiante
					          and    fecha_calc < current_date();
					
	declare continue handler for not found set  w_no_more_rows = 1; 
	set o_retorno = 0;  
	open cuentas_cursor;
    lbl_loop: loop
          
		fetch cuentas_cursor
		into w_secuencial,w_cuenta,w_tasa,w_saldo;

		if w_no_more_rows <> 0 then
			leave lbl_loop;
		end if;

         set w_interes = w_saldo*w_tasa/(365*100);
         
         update cuentas set intereses = ifnull(intereses,0) + round(w_interes,2),
							fecha_calc = current_date()
         where secuencial = w_secuencial;
         if row_count() <> 1 then
            leave lbl_loop;
			set o_retorno = 9999;
		 end if;

	end loop lbl_loop;

end
$$

select * from catalogo

